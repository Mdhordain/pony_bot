FROM node:16

WORKDIR /usr/src/pony_bot

COPY package*.json ./

RUN npm install

COPY . .

CMD [ "npm", "start" ]