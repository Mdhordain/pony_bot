import { Client } from "discord.js"

import process_message from "./message" 
import { exit } from "process"

import { get_discord_token } from "./config"

let discord_token = get_discord_token()

const client = new Client()

client.on("ready", async () =>
{
    console.log(`Logged in as ${client.user.tag}!`)
})

client.on("message", process_message)

async function start()
{
    try {
        await client.login(discord_token)
    }
    catch (err) {
        console.error(`Unable to connect to Discord (${err.message})`);
        exit(1)
    }
}
start()