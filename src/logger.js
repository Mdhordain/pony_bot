import { appendFile, existsSync, mkdirSync } from 'fs';

import { get_log_file } from "./config"

export async function append_logs(message)
{
    const log_file = get_log_file()

    const date = new Date()
    let log_message = `[${date.getFullYear()}-`
        + `${("0" + (date.getMonth() + 1)).slice(-2)}-`
        + `${("0" + date.getDate()).slice(-2)} `
        + `${("0" + date.getHours()).slice(-2)}:`
        + `${("0" + date.getMinutes()).slice(-2)}:`
        + `${("0" + date.getSeconds()).slice(-2)}] `
        + `${message}\n`
    appendFile(log_file, log_message, (err) => {
        if (err)
            return console.error(err);
    })
}