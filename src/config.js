import { existsSync, readFileSync } from 'fs'

const config_file = "./config/config.json"

let config = undefined

function config_file_exists()
{
    return existsSync(config_file)
}

function load_config()
{
    if (config != undefined) {
        return
    }
    if (!config_file_exists()) {
        throw `Config file ${config_file} does not exists`
    }
    try {
        config = JSON.parse(readFileSync(config_file))
    }
    catch (err) {
        config = undefined
        throw `Invalid configuration file (${err.message})`
    }
}

export function get_log_file() 
{
    load_config()

    if (!config.hasOwnProperty("logs")) {
        throw `No logs configuration in ${config_file}`
    }
    let logs_config = config.logs
    if (!logs_config.hasOwnProperty("filename") || !logs_config.hasOwnProperty("dir")) {
        throw `Invalid logs configuration, please check the example`
    }
    let log_file = `${logs_config.dir}/${logs_config.filename}.log`
    if (!existsSync(logs_config.dir)) {
        throw `Log dire ${logs_config.dir} does not exists`
    }
    return log_file
}

export function get_discord_token() 
{
    load_config()

    if (!config.hasOwnProperty("discord")) {
        throw `No discord configuration in ${config_file}`
    }
    let discord_config = config.discord
    if (!discord_config.hasOwnProperty("token")) {
        throw `No token in discord configuration`
    }
    return discord_config.token
}

export function get_derpibooru_token() 
{
    load_config()

    if (!config.hasOwnProperty("derpibooru")) {
        throw `No derpibooru configuration in ${config_file}`
    }
    let derpi_config = config.derpibooru
    if (!derpi_config.hasOwnProperty("token")) {
        throw `No token in derpibooru configuration`
    }
    return derpi_config.token
}