import poney from "./commands/poney/poney"
import { count_poney } from "./poney_counter"

const commands = 
{
    "poney": poney
}

function is_command(input) 
{
    const command_start_token = '?' 

    let command = input.split(" ")
    return input.startsWith(command_start_token) 
        && commands.hasOwnProperty(command[0].substring(1))
}

export default async function process_message(message)
{

    let command = message.content.split(" ")[0].substring(1)

    if (!message.author.bot) {
        if (is_command(message.content))
            commands[command](message)
        else if (count_poney(message.content.toLowerCase()) >= 1) {
            message.channel.send(`${message.author.toString()} tu as dis poney !`);
        }
    }
}
