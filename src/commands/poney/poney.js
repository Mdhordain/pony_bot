import got from 'got'

import { append_logs } from "../../logger"
import { get_random_pony_img, is_nsfw_pony }  from "./derpibooru";

const max_upload_size  = 8000000
const default_tags = "safe,cute,score.gte:400,-equestria%20girls,-anthro,-humanized,-human,-dragon"

const nsfw_messages = [
    "Ce n'est pas très approprié 😳",
    "Mais enfin, pensez donc aux enfants 😱",
    "UwU",
    "Les poneys c'est pour les calins, pas pour le sexe 😒",
    "Sale dégénéré 😡"
]

function random_nsfw_message() 
{
    let index = Math.floor(Math.random() * nsfw_messages.length)
    return nsfw_messages[index]
}

function extract_tags_from_input(message_content)
{
    return message_content.replace("?poney", "").trim()
}

function input_has_tags(message_content)
{
    return extract_tags_from_input(message_content) != ""
}

export default async function poney(message)
{
    try {
        let q_params = default_tags
        if (input_has_tags(message.content))
            q_params = extract_tags_from_input(message.content)
        let pony = await get_random_pony_img(q_params, max_upload_size)
        if (Object.keys(pony).length == 0) {
            append_logs(`Pony not found (tags: ${q_params}) on channel ${message.channel} (${message.channel.guild} : ${message.channel.name}) `)
            message.channel.send("Désolé, je n'ai pas trouvé de poneys avec ces tags 😢")
            return
        }
        append_logs(`Posting pony ${pony.id} (tags: ${q_params}) on channel ${message.channel} (${message.channel.guild} : ${message.channel.name})`)
        if (is_nsfw_pony(pony)) {
            message.channel.send(random_nsfw_message(), { files: [ pony.view_url ]})
        }
        else {
            message.channel.send("", { files: [ pony.view_url ]})
        }
    }
    catch (err) {
        append_logs(`ERROR : Can't get random pony image : ${err.message}`)
        message.channel.send(`Désolé ${message.author.toString()}, je n'ai pas trouvé d'image de poney, et je ne sais pas pourquoi...`)
        message.channel.send("Je suis un bot très mal foutu 😢")
    }
}