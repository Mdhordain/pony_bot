import got from 'got'

import { get_derpibooru_token } from "../../config";

const derpibooru_search_image_url = "https://derpibooru.org/api/v1/json/search/images"

const nsfw_tags = [ "explicit", "questionable", "suggestive" ]

function is_nsfw_tag(tag)
{
    return nsfw_tags.includes(tag.trim())
}

export function is_nsfw_pony(pony)
{
    let is_nsfw = false
    pony.tags.forEach(tag => {
        if (is_nsfw_tag(tag))
            is_nsfw = true
    })
    return is_nsfw
}

function search_has_nsfw_tags(q_params)
{
    let is_nsfw = false
    q_params.split(",").forEach(tag => {
        if (is_nsfw_tag(tag)) {
            is_nsfw = true
        }
    });
    return is_nsfw
}

function generate_search_url(req_params)
{
    return `${derpibooru_search_image_url}?filter_id=56027&q=${req_params.q}&sf=${req_params.sf}&sd=${req_params.sd}&per_page=${req_params.per_page}&key=${req_params.key}`
}


export async function get_random_pony_img(q_params, max_upload_size) 
{
    let pony = {}
    let api_key = get_derpibooru_token()
    if (!search_has_nsfw_tags(q_params)) {
        q_params += ",safe" // safe by default
    }
    q_params += `,size.lt:${max_upload_size}` // limit file size for Discord
    let search_url = generate_search_url({q: q_params, sf: "random", sd: "", per_page: 1, key: api_key}) 
    let derpibooru_response = await got(search_url).json()
    if (derpibooru_response.total > 0) {
        pony = derpibooru_response.images[0]
        pony.nsfw = false
        if (is_nsfw_pony(pony)) {
            pony.nsfw = true
        }
    }
    return pony
}
